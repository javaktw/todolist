package pl.sda.exceptions;

public class InvalidUserEmailException extends RuntimeException {
    public InvalidUserEmailException() {
        super("Użytkownik o podanym adresie e-mail istnieje!");
    }
}
