package pl.sda.services;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;

@Service
public class GeoLocationServiceImpl implements GeoLocationService {

    @Autowired
    private DatabaseReader databaseReader;

    @Override
    public String getCityNameFromRequest(HttpServletRequest request) {
        String clientIP = this.findClientIP(request);
        String cityName = this.getCityNameByIP(clientIP);
        return cityName;
    }

    private String getCityNameByIP(String ip) {
        String cityName;
        try {
            InetAddress ipAddress = InetAddress.getByName(ip);
            CityResponse city = databaseReader.city(ipAddress);
            cityName = city.getCity().getName();
        } catch (Exception e) {
            cityName = "Katowice";
            //e.printStackTrace();
        }
        return cityName;
    }

    private String findClientIP(HttpServletRequest request) {
        String remoteAddr = request.getHeader("X-FORWARDED-FOR");
        if (remoteAddr == null || remoteAddr.equals("")) {
            remoteAddr = request.getRemoteAddr();
        }
        return remoteAddr;
    }
}
