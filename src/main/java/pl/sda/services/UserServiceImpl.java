package pl.sda.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import pl.sda.dto.UserCreateForm;
import pl.sda.dto.UsersDto;
import pl.sda.entities.Role;
import pl.sda.entities.User;
import pl.sda.exceptions.InvalidPasswordException;
import pl.sda.exceptions.InvalidUserEmailException;
import pl.sda.repositories.RoleRepository;
import pl.sda.repositories.UserRepository;
import pl.sda.utils.PDFCreator;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findUserById(Integer id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public List<UsersDto> getUserList() {
        List<User> users = userRepository.findAll();

        List<UsersDto> userList = users.stream()
                .map(u -> new UsersDto(u.getId(),u.getName(), u.getSurname(), u.getEmail(), u.getRole().toString()))
                .collect(Collectors.toList());
        return userList;
    }

    @Override
    public void addAdminRole(Integer id) {
        User user = userRepository.findOne(id);
        HashSet<Role> roleHashSet = new HashSet<>();
        roleHashSet.add(roleRepository.findOne(1));
        roleHashSet.add(roleRepository.findOne(2));
        user.setRole(roleHashSet);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(String email) {
        userRepository.delete(findUserByEmail(email));
    }


    @Override
    public void saveUser(UserCreateForm userForm) {
        User user = new User();
        user.setName(userForm.getName());
        user.setSurname(userForm.getSurname());
        user.setEmail(userForm.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
        HashSet<Role> role = new HashSet<>();
        role.add(roleRepository.findOne(2));
        user.setRole(role);
        userRepository.save(user);
    }

    @Override
    public BindingResult validateUser(UserCreateForm user, BindingResult bindingResult) {
        try {
            this.validateUserEmail(user);
            this.validateUserPassword(user);
        } catch (InvalidUserEmailException ex) {
            bindingResult.rejectValue("email", "error.user", ex.getMessage());
        } catch (InvalidPasswordException ex) {
            bindingResult.rejectValue("confirmPassword", "error.user", ex.getMessage());
        }
        return bindingResult;
    }

    private void validateUserEmail(UserCreateForm user) throws InvalidUserEmailException {
        if (findUserByEmail(user.getEmail()) != null) {
            throw new InvalidUserEmailException();
        }
    }

    private void validateUserPassword(UserCreateForm user) throws InvalidPasswordException {
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            throw new InvalidPasswordException();
        }
    }
}
