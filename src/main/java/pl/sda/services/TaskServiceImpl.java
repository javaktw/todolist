package pl.sda.services;

import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import pl.sda.dto.TaskDto;
import pl.sda.entities.Task;
import pl.sda.entities.User;
import pl.sda.repositories.TaskRepository;
import pl.sda.repositories.UserRepository;
import pl.sda.utils.PDFCreator;
import pl.sda.utils.XLSexport;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PDFCreator pdfCreator;
    @Autowired
    private XLSexport xlsExport;
    @Autowired
    private SimpleDateFormat sdf;
    private Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Override
    public void saveTask(TaskDto taskDto) {
        Date dataStart = null;
        Date dataEnd = null;
        try {
            dataStart = sdf.parse(taskDto.getStartData());
            dataEnd = sdf.parse(taskDto.getEndData());
        } catch (Exception e) {
            log.error("Błąd parsowania daty");
            e.printStackTrace();
        }
        Task task = new Task(taskDto.getDescription(), dataStart, dataEnd, false);
        if (taskDto.getId() != null) {
            task.setId(taskDto.getId());
        }
        User user = userRepository.findUserByEmail(securityService.findLoggedInEmail());
        task.setUser(user);
        taskRepository.save(task);
    }

    @Override
    public List<TaskDto> getTasksByUser() {
        String loggedInEmail = securityService.findLoggedInEmail();
        List<Task> tasks = taskRepository.findAllByUser_Email(loggedInEmail);
        List<TaskDto> tasksDto = tasks.stream().map(t -> this.createTaskDto(t)).collect(Collectors.toList());
        return tasksDto;
    }

    @Override
    public void delete(Integer id) {
        taskRepository.delete(id);
    }

    @Override
    public TaskDto getTaskDto(Integer id) {
        String loggedInEmail = securityService.findLoggedInEmail();
        Task task = taskRepository.findByIdAndUser_Email(id, loggedInEmail);
        TaskDto taskDto = this.createTaskDto(task);
        return taskDto;
    }

    @Override
    public TaskDto createTaskDto(Task task) {
        return new TaskDto(task.getId(), task.getDescription(), sdf.format(task.getStartData()), sdf.format(task.getEndData()), task.isStatus());
    }

    @Override
    public void setTaskDone(Integer id) {
        String loggedInEmail = securityService.findLoggedInEmail();
        Task task = taskRepository.findByIdAndUser_Email(id, loggedInEmail);
        task.setStatus(true);
        taskRepository.save(task);
    }

    @Override
    public File getFileWithTasks(Integer typeOfExport) {
        String email = securityService.findLoggedInEmail();
        User user = userRepository.findUserByEmail(email);
        File file = null;
        if (user == null) {
            throw new RuntimeException("User not found");
        }
        try {
            if (typeOfExport == 1) {
                file = pdfCreator.createPDF(user);
            } else {
                file = xlsExport.exportXLS(user);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public String getContentType(Integer typeOfExport) {
        if (typeOfExport == 1) {
            return "application/pdf";
        } else {
            return "application/vnd.ms-excel";
        }
    }
}
