package pl.sda.configurations;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/css/**",
                        "/img/**",
                        "/js/**",
                        "/info/**",
                        "/contact/**",
                        "/webjars/**",
                        "/resources/**",
                        "/registration").permitAll()
                .antMatchers("/admin/users").hasAuthority("ROLE_ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll()
                .usernameParameter("email")
                .passwordParameter("password")
                .and()
                .logout().logoutUrl("/logout")
                .permitAll();
//                .antMatchers("/login", "/registration").permitAll()
//                .antMatchers("/layout").permitAll()
//                .antMatchers("/registration").permitAll()
//                .antMatchers("/user/**").hasAnyAuthority("ROLE_USER","ROLE_ADMIN")
//                .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
//                .anyRequest()
//                .authenticated().and().formLogin()
//                .loginPage("/login").permitAll()
//                .defaultSuccessUrl("/user/welcome").permitAll()
//                .usernameParameter("email")
//                .passwordParameter("password")
//                .and()
//                .exceptionHandling().accessDeniedPage("/access-denied")
//                .and()
//                .logout().logoutUrl("/logout").permitAll()
//                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//                .logoutSuccessUrl("/").and().exceptionHandling();
//                .accessDeniedPage("/access-denied");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
        auth.inMemoryAuthentication().withUser("a@a").password("admin").roles("ADMIN");
    }
}
