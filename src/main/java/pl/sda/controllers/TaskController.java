package pl.sda.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.TaskDto;
import pl.sda.services.TaskService;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    public ModelAndView showTasks(ModelMap map) {
        List<TaskDto> tasks = taskService.getTasksByUser();
        map.addAttribute("tasks", tasks);
        return new ModelAndView("/user/showtodo", map);
    }

    @GetMapping("/tasks/add")
    public ModelAndView taskAdd(ModelMap map) {
        map.addAttribute("taskDto", new TaskDto());
        return new ModelAndView("/user/addtask", map);
    }

    @PostMapping("/tasks/add")
    public ModelAndView saveTask(@ModelAttribute TaskDto taskDto, ModelMap modelMap) {
        taskService.saveTask(taskDto);
        return new ModelAndView("redirect:/tasks", modelMap);
    }

    @GetMapping("/tasks/delete")
    public String deleteTask(@RequestParam("id") Integer id) {
        taskService.delete(id);
        return "redirect:/tasks";
    }

    @GetMapping("/tasks/update")
    public ModelAndView showUpdatePage(@RequestParam("id") Integer id, ModelMap modelMap) {
        TaskDto taskDto = taskService.getTaskDto(id);
        modelMap.addAttribute("taskDto", taskDto);
        return new ModelAndView("/user/addtask", modelMap);
    }

    @GetMapping("/tasks/done")
    public ModelAndView setTaskDone(@RequestParam("id") Integer id) {
        taskService.setTaskDone(id);
        return new ModelAndView("redirect:/tasks");
    }

    @GetMapping("/tasks/export")
    public void exportTasks(@RequestParam("typeOfExport") Integer typeOfExport, HttpServletResponse response) throws IOException {
        File file = taskService.getFileWithTasks(typeOfExport);
        if (file != null) {
            response.setContentType(taskService.getContentType(typeOfExport));
            response.addHeader("Content-Disposition", "attachment; filename=" + file.getName());
            Files.copy(file.toPath(), response.getOutputStream());
            response.getOutputStream().flush();
            Files.delete(file.toPath());
        }
    }
}
