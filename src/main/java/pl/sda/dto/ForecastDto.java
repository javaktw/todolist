package pl.sda.dto;

public class ForecastDto {
    private String name;
    private double temp;
    private double humidity;
    private double speed;
    private double pressure;
    private String icon;

    public ForecastDto() {
    }

    public ForecastDto(String name, double temp, double humidity, double speed, double pressure, String icon) {
        this.name = name;
        this.temp = temp;
        this.humidity = humidity;
        this.speed = speed;
        this.pressure = pressure;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
